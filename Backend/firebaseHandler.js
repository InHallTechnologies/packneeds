// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";
import { getDatabase } from 'firebase/database';
import { getAuth } from 'firebase/auth';
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyDo8zPJp3mF8ZlIeU6gaM5vxaKto5OY0Yk",
  authDomain: "pack-needs.firebaseapp.com",
  databaseURL: "https://pack-needs-default-rtdb.firebaseio.com",
  projectId: "pack-needs",
  storageBucket: "pack-needs.appspot.com",
  messagingSenderId: "573987979144",
  appId: "1:573987979144:web:6536f0e050a34fc025843d",
  measurementId: "G-PPPJ7WNHD4"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
export const firebaseDatabase = getDatabase(app);
export const firebaseAuth = getAuth(app);

// const analytics = getAnalytics(app);