import axios from 'axios';
const handlePayment = async ({ name, phoneNumber, emailId, visibleBillAmount, orderId }) => {
    let finalAmount = ""

    for (const iterator of visibleBillAmount.toString()) {
        finalAmount += iterator === ','? "": iterator.toString()
    }

    finalAmount = parseFloat(finalAmount) * 100;
    const requestUrl = `https://us-central1-pack-needs.cloudfunctions.net/getOrder?orderAmount=${finalAmount}&receipt=${orderId}`
    const response = await axios.get(requestUrl);
    const { id } =  response.data;


    var options = {
        description: 'Order Payment',
        image: 'https://firebasestorage.googleapis.com/v0/b/pack-needs.appspot.com/o/logo.png?alt=media&token=9c975b8c-57c8-485d-8a8f-cd1d99345975',
        currency: 'INR',
        key: 'rzp_live_tXKAg7IHypogZo',
        amount: parseInt(finalAmount),
        name: 'PackNeeds.com',
        order_id: id,
        prefill: {
          email: emailId,
          contact: phoneNumber,
          name: name
        },
        theme: {color: '#004C8E'}
    }
      
    var rzp1 = new Razorpay(options);
    console.log(rzp1);
}

export default handlePayment