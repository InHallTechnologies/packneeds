export default {
    "date" : "",
    "orderId" : "",
    "orderStatus" : "",
    "orderTotal" : "",
    "paymentMethod" : "",
    "paymentStatus" : "",
    "placedBy" : "",
    "products" : [  ],
    "tax" : "0",
    "taxableAmount" : "",
    "time" : "",
    "transactionId" : ""
}
  