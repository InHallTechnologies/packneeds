// eslint-disable-next-line import/no-anonymous-default-export
export default {
    phoneNumber: "",
    alternatePhoneNumber: "",
    emailId: "",
    name: "",
    companyName: "",
    gst: "",
    address:'',
    city:'',
    state:'',
    pincode:'',
    country:'India',
    mode:'website'
};
