import { Modal, Button } from "react-bootstrap";
import { AiFillHome } from 'react-icons/ai';
import { signOut } from 'firebase/auth';
import { useRouter } from 'next/router'
import { firebaseAuth } from "../Backend/firebaseHandler";

const AccountExists = ({ visibility, setVisibility, phoneNumber }) => {
   
    const router = useRouter();

    const handleHide = () => {
        setVisibility(false);
    }

    const handleSignOut = () => {
        firebaseAuth.currentUser.signOut();
        router.push('/')

    }

    const handleHome = () => {
        router.push('/')
    }

  
    return (
        <Modal
            size="lg"
            aria-labelledby="contained-modal-title-vcenter"
            centered
            show={visibility}
            onHide={handleHide}
        >
            <Modal.Header>
                <Modal.Title id="contained-modal-title-vcenter">
                    Hold On!
                </Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <p>{`Account with phone number +91${phoneNumber} already exists`}</p>
            </Modal.Body>
            <Modal.Footer>
                <Button variant="secondary" onClick={handleSignOut}>SignOut</Button>
                <Button onClick={handleHome} style={{display:'flex', alignItems:'center'}} > 
                    <AiFillHome size="20" color="white" />
                    <span style={{marginLeft:'5px'}}>Go Home</span>
                </Button>
            </Modal.Footer>
        </Modal>
    );
};

export default AccountExists;
