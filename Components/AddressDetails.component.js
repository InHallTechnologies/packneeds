import React from "react";
import Styles from "../styles/PersonalDetails.module.scss";

const AddressDetails = ({ userData, setUserData }) => {


    const handleChange = (event) => {
        const { name, value } = event.target;
        setUserData({...userData, [name]:value})
    }

    return(
        <div className={Styles.container}>
            <div className={Styles.inputContainer}>
                <label className={Styles.inputLabel}>Address</label>
                <input className={Styles.inputField} value={userData.address} type='text' name="address" onChange={handleChange} />
            </div>

            <div className={Styles.inputContainer}>
                <label className={Styles.inputLabel}>City</label>
                <input className={Styles.inputField} value={userData.city} type='text' name="city" onChange={handleChange} />
            </div>

            <div className={Styles.inputContainer}>
                <label className={Styles.inputLabel}>State</label>
                <input className={Styles.inputField} value={userData.state} type='text' name="state" onChange={handleChange} />
            </div>

            <div className={Styles.inputContainer}>
                <label className={Styles.inputLabel}>Pincode</label>
                <input className={Styles.inputField} value={userData.pincode} type='number' name="pincode" onChange={handleChange} />
            </div>

            <div className={Styles.inputContainer}>
                <label className={Styles.inputLabel}>Country</label>
                <input className={Styles.inputField} value={"India"} type='text' />
            </div>
        </div>
    )
}

export default AddressDetails;