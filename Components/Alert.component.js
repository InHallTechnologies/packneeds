import React, { useEffect } from "react";
import { Alert } from 'react-bootstrap'
import Styles from '../styles/Alert.module.css';


const FloatingAlert = ({ alertMessage, setAlertText }) => {

    useEffect(() => {
        if (alertMessage){
            setTimeout(() => {
                setAlertText("")
            }, 3000);
        }
    }, [alertMessage]);

    if (alertMessage){
        return(
            <div className={Styles.alertContainer}>
                <Alert variant={alertMessage.type} onClose={_=>{setAlertText("")}} >
                    {alertMessage.message}
                 </Alert>
            </div>
        )
    }else {
        return null
    }

    
}

export default FloatingAlert;