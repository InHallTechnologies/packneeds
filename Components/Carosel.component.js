import { Carousel } from "react-bootstrap";
import { useEffect, useState } from "react";
import { firebaseDatabase } from "../Backend/firebaseHandler";
import { ref, get } from "firebase/database";

const Carosel = ({ images, caroselStyle }) => {
    const [data, setData] = useState([]);

    useEffect(() => {
        (async () => {
            const carouselRef = ref(firebaseDatabase, "CAROUSEL");
            const carouselSnapshot = await get(carouselRef);
            if (carouselSnapshot.exists()) {
                const data = [];
                for (const key in carouselSnapshot.val()) {
                    const url = carouselSnapshot.child(key).val();
                    data.push({ url, key });
                }
                setData(data);
            }
        })();
    }, []);
    return (
        <Carousel>
            {data.map((item) => {
                return (
                    <Carousel.Item key={item.key} >
                        <img
                            className={`d-block w-100 ${caroselStyle}`}
                            src={item.url}
                            alt="packneeds.com"
                        />
                    </Carousel.Item>
                );
            })}
        </Carousel>
    );
};

export default Carosel;
