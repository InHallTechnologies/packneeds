import { Button } from 'react-bootstrap';
import Styles from '../styles/CartItem.module.scss';
import { AiFillDelete } from 'react-icons/ai';
import { useState } from 'react';


const CartItem = ({ item, removeFromCart }) => {
    const { name, productImages, adminsPrice, moq, orderUnits, category, sellerInstructions, key } = item;
    const [loading, setLoading] = useState(false);

    const handleLoading = () => {
        setLoading(true);
        removeFromCart(key);
        setLoading(false);
    }

    return(
        <div className={Styles.container}>
            <img className={Styles.productPreview} src={productImages[0]} alt={name} />
            <div className={Styles.itemDetail}>
                <h5 className={Styles.itemName}>{name.toUpperCase()}</h5>
                <p className={Styles.quantity}>{orderUnits}  x  ₹ {adminsPrice} = <span className={Styles.total}>₹ {(parseFloat(orderUnits) * parseFloat(adminsPrice)).toFixed(2)}</span></p>
                {
                    sellerInstructions
                    ?
                    <div className={Styles.noteForSellerContainer}>
                        <span className={Styles.noteForSellerLabel}>Note for Seller:</span>
                        <p className={Styles.instruction}>{sellerInstructions}</p>
                    </div>
                    :
                    null

                }
            </div>
            <Button onClick={handleLoading} disabled={loading} className={Styles.deleteButton} variant="danger">
                <AiFillDelete size={18} color='white' />
                <span className={Styles.deleteLabel}>Remove</span>
            </Button> 


        </div>
    )
}

export default CartItem;