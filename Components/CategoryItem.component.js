import React from 'react';
import Styles from '../styles/CategoryItem.module.scss';

const CategoryItem = ({ item, handleClick }) => {
    const { name, image } = item;
    return(
        <div onClick={_ => handleClick(name)} className={Styles.container} style={{backgroundImage: `url(${image})`, backgroundSize:'cover'}}  >
            <span className={Styles.itemName}>{name}</span>
        </div>
    )
}

export default CategoryItem;