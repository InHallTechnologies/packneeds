import { useRouter } from 'next/dist/client/router';
import React from 'react';
import Styles from '../styles/CategoryList.module.scss';
import CategoryItem from './CategoryItem.component';

const CategoryList = ({ list }) => {
    const router = useRouter();

    const handleItemClick = (category) => {
        router.push(`/product-list?type=${category}`)
    }

    return(
        <div className={Styles.container}>
            {
                list.map(item => <CategoryItem key={item.id} handleClick={handleItemClick} item={item} />)
            }
        </div>
    )
}

export default CategoryList