import React, { useState } from "react";
import Styles from "../styles/PersonalDetails.module.scss";

const CompanyDetails = ({ userData, setUserData }) => {


    const handleChange = (event) => {
        const { name, value } = event.target;
        setUserData({...userData, [name]:value})
    }

    return(
        <div className={Styles.container}>
            <div className={Styles.inputContainer}>
                <label className={Styles.inputLabel}>Company Name (Optional)</label>
                <input className={Styles.inputField} value={userData.companyName} type='text' name="companyName" onChange={handleChange} />
            </div>

            <div className={Styles.inputContainer}>
                <label className={Styles.inputLabel}>Company GST (Optional)</label>
                <input className={Styles.inputField} value={userData.gst} type='text' name="gst" onChange={handleChange} />
            </div>
        </div>
    )
}

export default CompanyDetails;