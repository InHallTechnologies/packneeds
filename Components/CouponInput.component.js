import React, { useState } from "react";
import { Spinner, InputGroup, Button, FormControl } from "react-bootstrap";
import { ref, get } from "firebase/database";
import { firebaseDatabase } from "../Backend/firebaseHandler";

const CouponInput = ({ setCoupon }) => {
    const [loadingCoupon, setLoadingCoupon] = useState(false);
    const [couponCode, setCouponCode] = useState("");
    const [invalidCoupon, setInvalidCoupon] = useState(false);
    

    const handleCoupon = async () => {
        const couponRef = ref(firebaseDatabase, "COUPONS_LIST");
        setLoadingCoupon(true);
        const snapshot = await get(couponRef);
        if (snapshot.exists()) {
            let appliedCoupon = null
            for (const key in snapshot.val()) {
                const coupon = snapshot.child(key).val();
                const { code } = coupon;
                
                if (code.toUpperCase() === couponCode.toUpperCase()){
                    setCoupon(coupon);
                    appliedCoupon = coupon;
                    break
                }
            }

            if (!appliedCoupon){
                setInvalidCoupon(true);
            }
            setLoadingCoupon(false);
        }
    };

    const handleChange = (event) => {
        setInvalidCoupon(false);
        setCouponCode(event.target.value);
        setCoupon(null);
    };

    return (
        <div>
            <InputGroup style={{ paddingBottom: 0 }} className="mb-3">
                <FormControl
                    placeholder="Apply Coupon"
                    aria-label="Apply Coupon"
                    aria-describedby="basic-addon2"
                    value={couponCode}
                    onChange={handleChange}
                />
                <Button
                    onClick={handleCoupon}
                    variant="outline-secondary"
                    id="button-addon2"
                >
                    {loadingCoupon ? (
                        <Spinner animation="grow" size="sm" />
                    ) : (
                        "Apply"
                    )}
                </Button>
            </InputGroup>
            {invalidCoupon ? (
                <p style={{ marginTop: "-10px", color: "red", paddingLeft: 2 }}>
                    Invalid coupon code
                </p>
            ) : null}
        </div>
    );
};

export default CouponInput;
