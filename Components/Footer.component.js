import React from "react";
import Styles from '../styles/Footer.module.css';
import { FaFacebookSquare, FaInstagramSquare } from 'react-icons/fa';


const Footer = () => {
    return(
        <footer className={Styles.footerContainer}>
        <div className={Styles.footer}>
        <div>
          <h2 className={Styles.footerHeadline} >Want to sell on PackNeeds?</h2>
          <span>Download Or Visit</span>
          <div>
            <a href='https://play.google.com/store/apps/details?id=com.packneedsseller&pcampaignid=pcampaignidMKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1'>
            <img alt='Get it on Google Play' style={{width:'150px', marginLeft:'-8px'}} src='https://play.google.com/intl/en_us/badges/static/images/badges/en_badge_web_generic.png'/>
            </a>
          </div>
        </div>

        <div>
          <h2 className={Styles.footerHeadline} >Follow Us!</h2>
          <FaFacebookSquare size={25} color="black" />
          <FaInstagramSquare size={25} color="black" />
        </div>
        </div>

        <div className={Styles.copyrightContainer}>
          <span>Copyright © packneeds.com</span>
          <div>
            <span className={Styles.footerOption} >Terms of Service</span>
            <span className={Styles.footerOption} >Privacy Policy</span>
          </div>
        </div>

      </footer>
    )
}

export default Footer;