import React from 'react';
import Styles from '../styles/MainProduct.module.scss';

const MainProduct = ({ product, visitProduct }) => {
    const { productImages, name, adminsPrice, description, key } = product;
    return(
        <div className={Styles.container} onClick={_ => {visitProduct(key)}} >
            <img className={Styles.productImage} src={productImages[0]} alt={name} />
            <div className={Styles.productDetailContainer}>
                <span className={Styles.productName}>{name}</span>
                <span className={Styles.productCost}>Price: ₹ {adminsPrice} /unit</span>

                <span>
                    {description}
                </span>
            </div>
        </div>
    )
}

export default MainProduct;