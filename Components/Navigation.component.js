import React from "react";
import Styles from "../styles/Navigation.module.scss";
import { Button, Container, Nav, Navbar, NavDropdown } from "react-bootstrap";
import { firebaseAuth } from "../Backend/firebaseHandler";
import { signOut } from 'firebase/auth';
import router from 'next/router'
 
const Navigation = ({ currentActive }) => {
    
    const handleLogout = () => {
        signOut(firebaseAuth);
    }

    const handleLogin = () => {
        router.push("login")
    }

    if (!firebaseAuth.currentUser){
        return (
            <Navbar className={Styles.container} bg="white" expand="lg">
                <Container fluid>
                    <Navbar.Brand href="/">
                        <img src="logonew.png" className={Styles.icon} />
                    </Navbar.Brand>
                    <Navbar.Toggle aria-controls="navbarScroll" />
                    <Navbar.Collapse  id="navbarScroll">
                        <Nav
                            className="my-2 my-lg-0 navOptions"
                            navbarScroll
                            style={{width:"auto",  marginLeft:'auto'}}
                        >
                            {/* <Nav.Link active href='login' style={{color:"#000", fontSize:"20px", marginLeft:"20px"}} >
                                Login
                            </Nav.Link> */}
                            <Button onClick={handleLogin} style={{ backgroundColor:"#dc3545", border:'none' }} >Login</Button>

    
                            
                        </Nav>
                    </Navbar.Collapse>
                </Container>
            </Navbar>
        );
    }

    return (
        <Navbar className={Styles.container} bg="white" expand="lg">
            <Container fluid>
                <Navbar.Brand href="/">
                    <img src="logonew.png" className={Styles.icon} />
                </Navbar.Brand>
                <Navbar.Toggle aria-controls="navbarScroll" />
                <Navbar.Collapse  id="navbarScroll">
                    <Nav
                        className="my-2 my-lg-0 navOptions"
                        navbarScroll
                        style={{width:"auto",  marginLeft:'auto'}}
                    >
                        <Nav.Link
                            style={{color:"#000", fontSize:"20px", marginLeft:"20px", fontWeight: currentActive === "cart"?'bold':'', borderBottom: currentActive==='cart'?'1.5px solid rgb(207, 69, 57)': "1px solid white"}}
                            active={currentActive === "cart"}
                            href="cart"
                        >
                            CART
                        </Nav.Link>
                        <Nav.Link
                            active={currentActive === "orders"}
                            style={{color:"#000", fontSize:"20px", marginLeft:"20px",fontWeight: currentActive === "orders"?'bold':'',borderBottom: currentActive==='orders'?'1.5px solid rgb(207, 69, 57)': "1px solid white" }}
                            href="orders"
                        >
                            MY ORDERS
                        </Nav.Link>

                        <NavDropdown
                            title="ACCOUNT"
                            active
                            id="basic-nav-dropdown"
                            style={{color:"#000", fontSize:"20px", marginLeft:"20px"}} 
                        >
                            <NavDropdown.Item
                                style={{ zIndex: 10 }}
                                href="myAccount"
                                style={{color:"#000", fontSize:"20px"}}
                            >
                                MY ACCOUNT
                            </NavDropdown.Item>
                            <NavDropdown.Divider style={{ zIndex: 10 }} />
                            <NavDropdown.Item
                                style={{ zIndex: 10 }}
                                onClick={handleLogout}
                                style={{color:"#000", fontSize:"20px"}}
                            >
                                {firebaseAuth.currentUser ? (
                                    <span>LOGOUT</span>
                                ) : (
                                    <span>LOGIN/SIGN UP</span>
                                )}
                            </NavDropdown.Item>
                        </NavDropdown>
                    </Nav>
                </Navbar.Collapse>
            </Container>
        </Navbar>
    );
};

export default Navigation;
