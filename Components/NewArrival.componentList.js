import { useRouter } from 'next/dist/client/router';
import React from 'react';
import Styles from '../styles/NewArrivalList.module.scss';
import Product from './Product.component';

const NewArrivalList = ({ list }) => {
    const router = useRouter();
    
    const visitProduct = (id) => {
        router.push(`/product-detail?productId=${id}`)
    }

    return(
        <div className={Styles.container}>
            {
                list.map(item => <Product visitProduct={visitProduct} key={item.key} product={item} />)
            }
        </div>
    )
}

export default NewArrivalList;