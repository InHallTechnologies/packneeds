import { useState } from "react";
import { Modal, Button, InputGroup, FormControl } from "react-bootstrap";

const OtpVerificationModal = ({ visibility, setVisibility, phoneNumber, confirmation, handleSuccessLogin }) => {
    const [otp, setOtp] = useState("");
    const [errorText, setErrorText] = useState('');
    

    const handleHide = () => {
        setVisibility(false);
    }

    const handleSubmit = () => {
        confirmation.confirm(otp).then((result) => {
            handleHide();
            handleSuccessLogin();
          }).catch((error) => {
            setErrorText("Please enter a valid otp");
          });
    }

    const handleOtp = event => {
        setOtp(event.target.value)
        setErrorText("");
    }
    
    return (
        <Modal
            size="lg"
            aria-labelledby="contained-modal-title-vcenter"
            centered
            show={visibility}
            onHide={handleHide}
        >
            <Modal.Header>
                <Modal.Title id="contained-modal-title-vcenter">
                    Please enter OTP
                </Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <p>{`We have sent an OTP on +91${phoneNumber}`}</p>

                <InputGroup className="mb-3">
                    <FormControl
                        placeholder="Enter OTP here"
                        aria-label="Enter OTP here"
                        aria-describedby="basic-addon1"
                        value={otp}
                        onChange={handleOtp}
                    />
                </InputGroup>

                <p style={{color:'red', fontSize:'14px'}}>{errorText}</p>
            </Modal.Body>
            <Modal.Footer>
                <Button onClick={handleSubmit}>Submit</Button>
            </Modal.Footer>
        </Modal>
    );
};

export default OtpVerificationModal;
