import React from "react";
import Styles from "../styles/OrderArch.module.scss";
import { Card } from "react-bootstrap";
import numberWithCommas from "../Backend/commaFormat";
import OrderItems from "./OrderItems.component";
import TransactionEntry from "./TransactionEntry.component";
import { ref, set } from "firebase/database";
import { firebaseDatabase } from "../Backend/firebaseHandler";

const OrderArch = ({ order }) => {
    const {
        orderId,
        date,
        products,
        paymentMethod,
        orderTotal,
        orderStatus,
        transactionId,
        placedBy,
    } = order;

    const handleTransactionEntry = async (transaction) => {
        let orderRef = ref(
            firebaseDatabase,
            `ORDER_ARCHIVE/${placedBy}/${orderId}/transactionId`
        );
        await set(orderRef, transaction);

        orderRef = ref(
            firebaseDatabase,
            `GLOBAL_ORDERS_ARCHIVE/${orderId}/transactionId`
        );
        await set(orderRef, transaction);
    };

    return (
        <Card className={Styles.container}>
            <Card.Body>
                <div className={Styles.priceContainer}>
                    <h5 className={Styles.orderId}>Order Id: {orderId}</h5>
                    <h4 className={Styles.orderTotal}>
                        Order Total: ₹ {numberWithCommas(orderTotal)}
                    </h4>
                </div>
                <hr className={Styles.rule} />
                <p className={Styles.orderDetail}>
                    <span className={Styles.orderDetailKey}>Placed On:</span>
                    <span className={Styles.orderDetailValue}>{date}</span>
                </p>
                <p className={Styles.orderDetail}>
                    <span className={Styles.orderDetailKey}>Items:</span>
                    <span className={Styles.orderDetailValue}>
                        {products.length}
                    </span>
                </p>
                <p className={Styles.orderDetail}>
                    <span className={Styles.orderDetailKey}>
                        Payment Method:
                    </span>
                    <span className={Styles.orderDetailValue}>
                        {paymentMethod} {transactionId? `(${transactionId})`: null}
                    </span>
                </p>

                <p className={Styles.orderDetail}>
                    <span className={Styles.orderDetailKey}>Order Status:</span>
                    <span className={Styles.orderDetailValue}>
                        {orderStatus}
                    </span>
                </p>

                <div className={Styles.listActionContainer}>
                    <OrderItems orderItems={products} />

                    {paymentMethod === "Bank Transfer" ? (
                        transactionId ? null : (
                            <TransactionEntry
                                handleTransactionEntry={handleTransactionEntry}
                            />
                        )
                    ) : null}
                </div>
            </Card.Body>
        </Card>
    );
};

export default OrderArch;
