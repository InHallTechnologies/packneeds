import React, { useState } from "react";
import { Modal, Button, ListGroup, Image, Card } from "react-bootstrap";
import Styles from "../styles/OrderArchActions.module.scss";

const OrderItems = ({ orderItems }) => {
    const [visibility, setVisibility] = useState(false);
    const closeModal = () => {
        setVisibility(false);
    };

    const showModal = () => {
        setVisibility(true);
    };
    const visitMyOrders = () => {};

    return (
        <div className={Styles.container}>
            <Card.Link onClick={showModal} className={Styles.listAction}>
                Order Items
            </Card.Link>
            <Modal
                show={visibility}
                onHide={closeModal}
                size="lg"
                aria-labelledby="contained-modal-title-vcenter"
                centered
            >
                <Modal.Header>
                    <Modal.Title id="contained-modal-title-vcenter">
                        Order Items
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <ListGroup as="ol" numbered>
                        {orderItems.map(
                            ({
                                name,
                                adminsPrice,
                                orderUnits,
                                key,
                                productImages,
                                sellerInstructions
                            }) => {
                                return (
                                    <ListGroup.Item
                                        as="li"
                                        className="d-flex justify-content-between align-items-start"
                                        key={key}
                                    >
                                        <Image
                                            style={{
                                                width: "85px",
                                                height: "85px",
                                            }}
                                            alt={name}
                                            src={productImages[0]}
                                            thumbnail
                                        />

                                        <div className="ms-2 me-auto">
                                            <div className="fw-bold">
                                                {name}
                                            </div>
                                            ₹ {adminsPrice} x {orderUnits} = ₹{" "}
                                            {parseFloat(adminsPrice) *
                                                parseFloat(orderUnits)}
                                                {
                                                    sellerInstructions
                                                    ?
                                                    <div style={{marginTop:'5px'}} className="fw"><strong>Instruction for Seller: </strong>{sellerInstructions}</div>
                                                    :
                                                    null
                                                }
                                        </div>
                                    </ListGroup.Item>
                                );
                            }
                        )}
                    </ListGroup>
                </Modal.Body>
                <Modal.Footer>
                    <Button onClick={closeModal}>OK</Button>
                </Modal.Footer>
            </Modal>
        </div>
    );
};

export default OrderItems;
