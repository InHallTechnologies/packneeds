import React from "react";
import { Modal, Button } from "react-bootstrap";
import router from 'next/router';

const OrderPlaced = ({ visibility, setVisibility }) => {
    const closeModal = () => {
        setVisibility(true);
    }

    const visitMyOrders = () => {
        router.push("/orders")
    }
    return (
        <Modal
            show={visibility}
            onHide={closeModal}
            size="lg"
            aria-labelledby="contained-modal-title-vcenter"
            centered
        >
            <Modal.Header>
                <Modal.Title id="contained-modal-title-vcenter">
                    Order Placed!
                </Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <p>
                    You have successfully placed order. Please visit my orders for more updates on your order
                </p>
            </Modal.Body>
            <Modal.Footer>
                <Button onClick={visitMyOrders}>My Orders</Button>
            </Modal.Footer>
        </Modal>
    );
};


export default OrderPlaced;