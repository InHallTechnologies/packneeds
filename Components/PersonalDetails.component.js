import React, { useState } from "react";
import Styles from "../styles/PersonalDetails.module.scss";

const PersonalDetails = ({ userData, setUserData  }) => {


    const handleChange = (event) => {
        const { name, value } = event.target;
        setUserData({...userData, [name]:value})
    }

    return(
        <div className={Styles.container}>
            <div className={Styles.inputContainer}>
                <label className={Styles.inputLabel}>Name</label>
                <input className={Styles.inputField} value={userData.name} type='text' name="name" onChange={handleChange} />
            </div>

            <div className={Styles.inputContainer}>
                <label className={Styles.inputLabel}>Phone Number</label>
                <input className={Styles.inputField} placeholder="+91" value={userData.phoneNumber} type='tel' name="phoneNumber" onChange={handleChange} />
            </div>

            <div className={Styles.inputContainer}>
                <label className={Styles.inputLabel}>Alternate Phone Number (Optional)</label>
                <input className={Styles.inputField} placeholder="+91" value={userData.alternatePhoneNumber} name="alternatePhoneNumber" type='tel' onChange={handleChange} />
            </div>

            <div className={Styles.inputContainer}>
                <label className={Styles.inputLabel}>Email Id (Optional)</label>
                <input className={Styles.inputField} placeholder="user@example.com" value={userData.emailId} name="emailId" type='email' onChange={handleChange} />
            </div>
        </div>
    )
}

export default PersonalDetails;