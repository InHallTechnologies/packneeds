import React from 'react';
import Styles from '../styles/Product.module.scss';

const Product = ({ product, visitProduct }) => {
    const { productImages, name, adminsPrice, key } = product;
    return(
        <div className={Styles.container} onClick={_ => visitProduct(key)} >
            <img className={Styles.productImage} src={productImages[0]} alt={name} />
            <span className={Styles.productName}>{name}</span>
            <span className={Styles.productCost}>Price: ₹ {adminsPrice} /unit</span>
        </div>
    )
}

export default Product;