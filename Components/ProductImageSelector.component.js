import React, { useState } from "react";
import Styles from '../styles/ProductImageSelector.module.scss';

const ProductImageSelector = ({imageList, productName}) => {
    const [currentState, setCurrentState] = useState({
        list: imageList,
        currentIndex:0
    })

    const handleImageSelect = (index) => {
        console.log(index)
        setCurrentState({...currentState, currentIndex: index})
    }
    
    return(
        <div className={Styles.container}>
            <img className={Styles.preview} src={currentState.list[currentState.currentIndex]} alt={productName} />
            
            <div className={Styles.thumbnailsContainer}>
                {
                    imageList.map((item, index) => <img key={item} onClick={_ => handleImageSelect(index)} className={Styles.miniPreview} src={item} alt={productName} />)
                }
            </div>

            <div>
                
            </div>
        </div>
    )
}

export default ProductImageSelector