import React, { useEffect, useState } from "react";
import Styles from '../styles/RecomendedProducts.module.scss';
import { ref, get } from 'firebase/database';
import { firebaseDatabase } from "../Backend/firebaseHandler";
import Product from "./Product.component";
import { useRouter } from "next/router";

const RecommendedProducts = ({ category }) => {
    const [loading, setLoading] = useState(true);
    const [productList, setProductList] = useState([]);
    const router = useRouter();

    useEffect(() => {
        const listRef = ref(firebaseDatabase, "LIVE_PRODUCTS");
        get(listRef).then(snapshot => {
            if (snapshot.exists()){
                const data = [];
                for (const key in snapshot.val()) {
                    const product = snapshot.child(key).val();
                    if (product.category === category){
                        data.push(product);
                    }
                }
                setProductList(data);
            
            }
            setLoading(false);
        })
    }, []);

    const handleProductClick = (id) => {
        router.push(`/product-detail?productId=${id}`)
    }

    return(
        <div className={Styles.container}>
            <h2>More in {category}</h2>

            {
                loading
                ?
                <img className={Styles.loader} src='/loader.gif' alt="Loading"  />
                :
                <div className={Styles.productList}>
                    {
                        productList.map(item => <div key={item.key} className={Styles.productMainContainer}><Product visitProduct={handleProductClick} product={item} /></div>)
                    }
                </div>
            }

            
        </div>
    )
}

export default RecommendedProducts;