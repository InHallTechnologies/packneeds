import React, { useState } from "react";
import {
    Modal,
    Button,
    Card,
    Form,
    InputGroup,
    FormControl,
    Spinner,
} from "react-bootstrap";
import Styles from "../styles/OrderArchActions.module.scss";

const TransactionEntry = ({ handleTransactionEntry }) => {
    const [visibility, setVisibility] = useState(false);
    const [updating, setUpdating] = useState(false);
    const [transaction, setTransactionId] = useState("");
    
    const closeModal = () => {
        setVisibility(false);
    };

    const showModal = () => {
        setVisibility(true);
    };
    const updateTransaction = async () => {
        setUpdating(true);
        await handleTransactionEntry(transaction);
        setUpdating(false);
    }

    return (
        <div className={Styles.container}>
            <Card.Link onClick={showModal} className={Styles.listAction}>
                Enter transaction id
            </Card.Link>
            <Modal
                show={visibility}
                onHide={closeModal}
                size="lg"
                aria-labelledby="contained-modal-title-vcenter"
                centered
            >
                <Modal.Header>
                    <Modal.Title id="contained-modal-title-vcenter">
                        Enter transaction id
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form.Label htmlFor="basic-url">Transaction Id</Form.Label>
                    <InputGroup className="mb-3">
                        <FormControl
                            id="basic-url"
                            aria-describedby="basic-addon3"
                            placeholder="Enter here"
                            value={transaction}
                            onChange={event => setTransactionId(event.target.value)}
                        />
                    </InputGroup>
                </Modal.Body>
                <Modal.Footer>
                    <Button disabled={updating || transaction.length == 0} onClick={updateTransaction}>
                        {updating ? (
                            <Spinner animation="grow" size="sm" />
                        ) : (
                            <span>Submit</span>
                        )}
                    </Button>
                </Modal.Footer>
            </Modal>
        </div>
    );
};

export default TransactionEntry;
