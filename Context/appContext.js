import { createContext, useReducer } from 'react';
import { GET_GLOBAL_DATA, SET_GLOBAL_DATA } from './contextTypes';

const Context = createContext();


const reducer = (state, action) => {
    switch(action.type){
        case SET_GLOBAL_DATA: {
            return {
                ...state,
                userData: action.payload
            }
        }

        case GET_GLOBAL_DATA: {
            return {
                ...state
            }
        }
    }
}

export const Provider = ({ children }) => {
    const [globalData, dispatchForGlobalData] = useReducer(reducer, {});
    return(
        <Context.Provider value={[globalData, dispatchForGlobalData]} >
            {children}
        </Context.Provider>
    )
} 

export default Context;