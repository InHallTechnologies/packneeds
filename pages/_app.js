import { useEffect, useState } from "react";
import { Provider } from "../Context/appContext";
import "../styles/globals.css";
import { onAuthStateChanged } from "firebase/auth";
import { firebaseAuth, firebaseDatabase } from "../Backend/firebaseHandler";
import { ref, get } from "firebase/database";
import router from "next/router";
import Script from "next/script";

function MyApp({ Component, pageProps }) {
    const [userData, setUserData] = useState();

    useEffect(() => {
        onAuthStateChanged(firebaseAuth, handleAuth);

        return () => {
            onAuthStateChanged(firebaseAuth, () => {});
        };
    }, []);

    const handleAuth = async (user) => {
        if (user) {
            const userReference = ref(
                firebaseDatabase,
                `USER_ARCHIVE/${user.phoneNumber}`
            );
            const userDataReference = await get(userReference);
            if (userDataReference.exists()) {
                const userData = await userDataReference.val();
                setUserData(userData);
            } else {
                router.push("signup");
            }
        } else {
            setUserData({});
        }
    };
    return (
        <Provider>
            <Script
                src="https://unpkg.com/react/umd/react.production.min.js"
                crossOrigin
            ></Script>

            <Script
                src="https://unpkg.com/react-dom/umd/react-dom.production.min.js"
                crossOrigin
            ></Script>

            <Script
                src="https://unpkg.com/react-bootstrap@next/dist/react-bootstrap.min.js"
                crossOrigin
            ></Script>
            <Component
                {...pageProps}
                userData={userData}
                setUserData={setUserData}
            />
        </Provider>
    );
}

export default MyApp;
