import PDFDocument from 'pdfkit';
import axios from 'axios';
import { firebaseDatabase } from '../../Backend/firebaseHandler';
import { ref, get } from 'firebase/database';



export default async function handler(req, res) {
    const doc = new PDFDocument({size: 'A4'});
    const { orderIdentification } = req.query;
    if (!orderIdentification || orderIdentification === null || orderIdentification === undefined){
      res.send("Invalid Request");
      return;
    }

    const orderRef = ref(firebaseDatabase,`BUYER_ORDER_BUFFER/${orderIdentification}`);
    const order = (await get(orderRef)).val() ;
    const { orderId, date, orderStatus, paymentMethod, buyerName, buyerNum, buyerAddress, products, formattedTotal } = order;

    doc.font("Helvetica")
    const fileName = `${orderIdentification}.pdf`;
    res.setHeader('Content-disposition', 'attachment; filename="' + fileName + '"')
    res.setHeader('Content-type', 'application/pdf')
    
    const logo = await fetchImage("https://packneeds.com/logonew.png");
    doc.image(logo, 20, 45, {width: 100})

    doc
      .fontSize(20)
      .text('Order Details', 20, 100);
  
    doc.fontSize(15).text(`${orderId}`, 0, 105, {
      align: 'right'
    })
    doc.fontSize(12).text(`Order Date: ${date}`,20, 140)
    doc.fontSize(12).text(`Status: ${orderStatus === "Placed"? "New":orderStatus}`,0, 140, {align: 'right'})
    doc.fontSize(12).text(`Mode Of Payment: ${paymentMethod}`,20, 160)
    doc.fontSize(15).text(`Order Amount: Rs. ${formattedTotal}`,0, 160, {align: 'right'})
  
    doc
      .fontSize(16)
      .text('About Buyer', 20, 200);
    doc.fontSize(12).text(`${buyerName}, ${buyerNum}`,20, 230)
    doc.fontSize(12).text(`Address: ${buyerAddress}`,20, 250)
  
    doc
      .fontSize(16)
      .text('Order Items ', 20, 290);

  
    let baseHeight = 330
    

    for(let i =0 ;i < products.length; i++){
      const product = products[i];
     
      const productImage = await fetchImage(product.productImages[0]);
      doc.image(productImage, 20, baseHeight, {width: 100,height: 100});
      doc.fontSize(12).text(`${product.name}`, 150, baseHeight+ 10)
      doc.fontSize(10).text(`${product.category}`, 150, baseHeight + 25)
      doc.fontSize(12).text(`Order Qty: ${product.orderUnits}`, 150, baseHeight + 50)
      doc.font("Helvetica-Bold").fontSize(12).text(`Admin's Price: Rs. ${product.adminsPrice} | Seller's Price: Rs. ${product.sellersPrice}`, 150, baseHeight + 70)
      doc.font('Helvetica')
      doc.fontSize(12).text(`Note to seller: ${product.sellerInstructions?product.sellerInstructions:"NA"}`, 20, baseHeight + 110)
      doc.fontSize(12).text(`Sold by: ${product.sellerName}, ${product.sellerNum}`, 20, baseHeight + 130)
      baseHeight += 170
      if (baseHeight >= 661.89){
        doc.addPage()
        baseHeight = 30
      }
    }
    doc.end();
    doc.pipe(res);
    
}

async function fetchImage(src) {
    const image = await axios.get(src, {responseType:'arraybuffer'})
    
    return image.data;
}

