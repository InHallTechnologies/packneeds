import { ref, get, } from "firebase/database";
import { firebaseDatabase } from "../../Backend/firebaseHandler";

export default async function handler(req, res) {
    const { productId } = req.query;
    console.log(productId);
    const productsRef = ref(firebaseDatabase, `LIVE_PRODUCTS/${productId}`);
    const productsSnapshot = await get(productsRef);
    const product = await productsSnapshot.val();
   
    res.send({
        product
    })
}