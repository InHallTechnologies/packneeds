import { ref, get, limitToLast, query } from "firebase/database";
import { firebaseDatabase } from "../../Backend/firebaseHandler";

export default async function handler(req, res) {
    const { type } = req.query;
    const productsRef = ref(firebaseDatabase, `LIVE_PRODUCTS`);
    const productsQuery = query(productsRef);
    let productList = [];
    switch(type){
        case "New Arrival": {
            const listSnapshot = await get(productsQuery);
            productList = getProductList(listSnapshot);
            break;
        }

        default: {
            const listSnapshot = await get(productsQuery);
            productList = getProductList(listSnapshot, type);
        }
    }

    res.send({
        productList
    })
}


const getProductList = (listSnapshot, category="") => {
    if (listSnapshot.exists()){
        const data = [];
        for (const key in listSnapshot.val()) {
            const product = listSnapshot.child(key).val();
            
            if (category){
                if (product.category === category){
                    data.push(product);
                }
            }else {
                data.push(product);
            }
           
        }

        return data;
    }
}