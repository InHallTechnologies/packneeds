import { ref, get, limitToLast, query } from 'firebase/database';
import { firebaseDatabase } from '../../Backend/firebaseHandler';

export default async function handler(req, res) {
  const categoryReference = ref(firebaseDatabase, "CATEGORIES_LIST");
  const categoryRef = await get(categoryReference);

  const productsReference = ref(firebaseDatabase, "LIVE_PRODUCTS");
  const latestProductsRef = query(productsReference, limitToLast(10));
  const latestProductsSnapshot = await get(latestProductsRef);

  let categories = [];
  let products = [];
  
  if (categoryRef.exists()){
      for (const key in categoryRef.val()) {
        const listItem = categoryRef.child(key).val();
        categories.push(listItem)
      }
  }

 
  if (latestProductsSnapshot.exists()){
    for (const key in latestProductsSnapshot.val()) {
      const product = latestProductsSnapshot.child(key).val();
      products.push(product)
    }
  }



  res.send({
    categories,
    products
  })
}
