import React, { useEffect, useState } from 'react';
import Navigation from '../../Components/Navigation.component';
import Styles from '../../styles/Cart.module.scss';
import Script from 'next/script'
import Head from 'next/head';
import { ref, get, push, remove, set } from 'firebase/database';
import { firebaseDatabase } from '../../Backend/firebaseHandler';
import { Spinner, Button } from 'react-bootstrap';
import CartItem from '../../Components/CartItem.component';
import orderDetails from '../../Backend/orderDetails';
import CouponInput from '../../Components/CouponInput.component';
import FloatingAlert from '../../Components/Alert.component';
import { dateString, timeString } from '../../Backend/getTimeStamp';
import OrderPlaced from '../../Components/OrderPlaced.component';
import axios from 'axios';
import serverType from '../../Backend/serverType';

const CartPage = ({ userData }) => {
    const [cartItems, setCartItems] = useState([]);
    const [loading, setLoading] = useState(true);
    const [emptyCart, setEmptyCart] = useState(false);
    const [sampleOrder, setSampleOrder] = useState({...orderDetails})
    const [orderValues, setOrderValues] = useState({totalAmount:'', tax:"", billAmount:"", discountAmount:""})
    const [coupon, setCoupon] = useState(null);
    const [alertText, setAlertText] = useState({message:'', type:""});
    const [processingOrder, setProcessingOrder] = useState(false);
    const [orderPlaced, setOrderPlaced] = useState(false);
    const [accountDetail, setAccountDetail] = useState({});

    useEffect(() => {
        if (!userData){
            return;
        }
        ( async () => {
            const cartItemReference = ref( firebaseDatabase, `CART_ARCHIVE/${userData.phoneNumber}`);
            const cartSnapshot = await get(cartItemReference);
            if (cartSnapshot.exists()) {
                const data = [];
                for (const key in cartSnapshot.val()) {
                    const item = cartSnapshot.child(key).val();
                    data.push(item);
                }
                setCartItems(data);
                setEmptyCart(false);
            }else {
                setEmptyCart(true);
            }
            setLoading(false);
        })()
        
        if (!accountDetail.accountNumber){
        const bankDetailRef = ref( firebaseDatabase, `ACCOUNT_DETAILS/`);
        get(bankDetailRef).then(snapshot => {
            if (snapshot.exists()){
                setAccountDetail(snapshot.val());
                
            }
        })
    }
    }, [userData, coupon])


    useEffect(() => {
        let totalAmount = 0, tax = 0, billAmount = 0, discountAmount="", applied="NO";
        for (const cartItem of cartItems) {
            const { orderUnits, adminsPrice } =  cartItem
            totalAmount += parseFloat(orderUnits) * parseFloat(adminsPrice)
        }
        if (coupon){
            let tempTotalAmount = calculateDiscount(parseFloat(totalAmount));
            if (tempTotalAmount || tempTotalAmount === 0){
                totalAmount = tempTotalAmount;
                applied = "YES";
            }else {
                totalAmount = totalAmount;
                applied = "INVALID";
            }
            const { type, discountValue } = coupon;
            
            
            discountAmount = type === "Percent"? `${discountValue}%`:`₹ ${discountValue}` 
           
        }
        console.log(totalAmount);
        tax = 0.18 * totalAmount;
        billAmount = Math.round(tax + totalAmount);
        if (applied === "YES"){
            handleAppliedCoupon("Coupon Applied", "success")
        }else if (applied === "INVALID"){
            handleAppliedCoupon(`Minimum bill amount must be at least ${coupon.minimumAmount}`)
            discountAmount = "";
        }
        
        setOrderValues({totalAmount, tax, billAmount, discountAmount})
       
    }, [cartItems])

    const calculateDiscount = (totalAmount) => {
        const { minApplicable, minimumAmount, type, discountValue } = coupon;
        
        if (minApplicable){
            if (totalAmount >= parseFloat(minimumAmount)){
                return getBillAmount(type, totalAmount, discountValue)
            }
        }else {
            return getBillAmount(type, totalAmount, discountValue)
        }
    }

    const getBillAmount = (type, totalAmount, discountValue) => {
        if (type === "Amount"){
            
            let temp = totalAmount - parseFloat(discountValue);
            temp = temp < 0 ? 0 : temp;
            return  temp;
        }else {
            const temp = totalAmount - ((parseFloat(discountValue)/ 100) * totalAmount)
            return temp < 0 ? 0 : temp ;
        }
    }

    const handlePaymentSelection = (event) => {
        const { value } = event.target;
        setSampleOrder({...sampleOrder, paymentMethod: value})
    }

    const handleCheckout = async () => {
        // handlePayment({name:"Rishabh", emailId:'rv.rishabhverma1996@gmail.com', visibleBillAmount:"200", phoneNumber:"+919845570915", orderId:"sampleOrdr"})
        if (!sampleOrder.paymentMethod){
            setAlertText({message:"Select payment method", type:"warning"});
            return
        }
        if (cartItems.length === 0){
            setAlertText({message:"Add some products to cart", type:"warning"});
            return;
        }
        
        let orderRef = ref(firebaseDatabase, `ORDER_ARCHIVE/${userData.phoneNumber}`);
        const orderId = push(orderRef).key;
        const order = {
            ...sampleOrder,
            orderId,
            date: dateString,
            orderStatus:"Placed",
            orderTotal: orderValues.billAmount.toString(),
            paymentStatus: sampleOrder.paymentMethod==="Bank Transfer"?"Pending":'',
            placedBy:userData.phoneNumber,
            products:cartItems,
            time: timeString
        }
        setProcessingOrder(true);
        if (order.paymentMethod === "Payment Gateway"){
            await handlePayment(order, userData, orderId, orderRef);
            setProcessingOrder(false);
        }else {
            await placeOrder(userData, orderId, order, orderRef)
        }
    }

    const placeOrder = async (userData, orderId, order, orderRef) => {
        orderRef = ref(firebaseDatabase, `ORDER_ARCHIVE/${userData.phoneNumber}/${orderId}`);
        await set(orderRef, order)
        orderRef = ref(firebaseDatabase, `GLOBAL_ORDERS_ARCHIVE/${orderId}`);
        await set(orderRef, order)
        setProcessingOrder(false);

        orderRef = ref(firebaseDatabase, `CART_ARCHIVE/${userData.phoneNumber}`);
        await remove(orderRef) 
        setAlertText({message:"Order Placed", type:'success'})
        setOrderPlaced(true);
    }

    const handlePayment = async (order, userData, orderId, orderRef) => {
        let finalAmount = ""

        for (const iterator of order.orderTotal.toString()) {
            finalAmount += iterator === ','? "": iterator.toString()
        }
        finalAmount = parseFloat(finalAmount) * 100;
        const requestUrl = `${serverType}api/getPaymentGatewayOrderId?orderAmount=${finalAmount}&receipt=${order.orderId}`
        const response = await axios.get(requestUrl);
        const { id } = await response.data;
        var options = {
            "key": "rzp_live_tXKAg7IHypogZo", // Enter the Key ID generated from the Dashboard
            "amount": parseInt(finalAmount), // Amount is in currency subunits. Default currency is INR. Hence, 50000 refers to 50000 paise
            "currency": "INR",
            "name": "PackNeeds.com",
            "description": "Order Payment",
            "image": "https://firebasestorage.googleapis.com/v0/b/pack-needs.appspot.com/o/logo.png?alt=media&token=9c975b8c-57c8-485d-8a8f-cd1d99345975",
            "order_id": id, //This is a sample Order ID. Pass the `id` obtained in the response of Step 1
            "prefill": {
                "name": `${userData.name}`,
                "email": `${userData.emailId}`,
                "contact": `${userData.phoneNumber}`
            },
            "theme": {
                "color": "#3399cc"
            },
            "handler": async function (response) {
                console.log(response);
                const paymentDetailsRef = ref(firebaseDatabase, `PAYMENT_INFORMATION/${orderId}`);
                await set(paymentDetailsRef,{paymentId: response.razorpay_payment_id, orderId, signature: response.razorpay_signature})
                await placeOrder(userData, orderId, order, orderRef)
            }
        };
        var rzp1 = new Razorpay(options);
        
        rzp1.on('payment.failed', function (response){
            setProcessingOrder(false);
            setAlertText({message:"Payment failed", type:'danger'})
            console.log("i am here broooo")
        });
        
        
        await rzp1.open();
        
    }

    function numberWithCommas(x) {
        let temp = parseFloat(x);
        temp = temp.toFixed(2);
        
        return temp.toString().replace(/\B(?=(?:(\d\d)+(\d)(?!\d))+(?!\d))/g, ',');
    }

    const handleAppliedCoupon = (text, variant) => {
        setAlertText({message:text, type:variant })
    }

    const removeFromCart = async (itemId) => {
        const cartItemReference = ref( firebaseDatabase, `CART_ARCHIVE/${userData.phoneNumber}/${itemId}`);
        await remove(cartItemReference)
        const CartItem = cartItems.filter(item => item.key !== itemId);
        setCartItems(CartItem);
    }
    

    return(
        <div className={Styles.container}>
            <Head>
                <title>Cart | Pack Needs</title>
            </Head>
            <Script src="https://checkout.razorpay.com/v1/checkout.js"></Script>

            <Navigation currentActive='cart' />
            

            <div className={Styles.content}>

                <div className={Styles.leftSide}>
                <div className={Styles.itemListContainer}>
                    <h3 className={Styles.sectionTitle} >Select Payment Method</h3>
                    <div className={Styles.paymentMethodContainer}>
                        <input onChange={handlePaymentSelection} type="radio" id="cod" name="paymentMethod" value="COD" />
                        <label className={Styles.methodName} htmlFor="cod">Cash On Delivery (COD)</label>
                    </div>

                    <div className={Styles.bankPaymentMethodContainer}>
                        <div>
                            <input onChange={handlePaymentSelection} type="radio" id="cod" name="paymentMethod" value="Bank Transfer" />
                            <label className={Styles.methodName} htmlFor="cod">Bank Transfer</label>    
                        </div>

                        <div className={Styles.bankDetailsContainer}>
                            <p className={Styles.bankDetailLabel}><strong>Bank Details</strong></p>
                            <p className={Styles.bankDetail}>Account Number: {accountDetail.accountNumber}</p>
                            <p className={Styles.bankDetail}>Account Type: {accountDetail.accountType}</p>
                            <p className={Styles.bankDetail}>IFSC: {accountDetail.ifsc}</p>
                            <p className={Styles.bankDetail}>Bank Name: {accountDetail.bankName}</p>
                            <p className={Styles.bankDetail}>Account Holder Name: {accountDetail.holderName}</p>


                        </div>
                        
                    </div>

                    <div className={Styles.paymentMethodContainer}>
                        <input onChange={handlePaymentSelection} type="radio" id="cod" name="paymentMethod" value="Payment Gateway" />
                        <label className={Styles.methodName} htmlFor="cod">Online Payment</label>
                    </div>
                    
                </div>

                <div style={{overflowY:'scroll'}} className={Styles.itemListContainer}>
                    <h3 className={Styles.sectionTitle} >Cart Items</h3>
                    {
                        loading
                        ?
                        <div className={Styles.loaderContainer}>
                            <Spinner animation="border" />
                        </div>
                        :
                        null
                    }

                    {
                        emptyCart
                        ?
                        <div className={Styles.loaderContainer}>
                            <p style={{color:'#666', fontWeight:'bold'}}>Empty Cart</p>
                        </div>
                        :
                        null
                    }

                    <div className={Styles.cartList}>
                    {
                        cartItems.map(item => <CartItem removeFromCart={removeFromCart} key={item.key} item={item} />)
                    }
                    </div>
                </div>
                </div>



                <div className={Styles.itemActions}>
                    <div className={Styles.orderDetail}><p className={Styles.orderDetailLabel}>Number Of Items: </p> <span className={Styles.orderDetailValue} >{cartItems.length}</span></div>
                    <div className={Styles.orderDetail}><p className={Styles.orderDetailLabel}>Total Amount: </p><span className={Styles.orderDetailValue} >₹ {numberWithCommas(orderValues.totalAmount)}</span></div>
                    {
                        orderValues.discountAmount
                        ?
                        <div className={Styles.orderDetail}><p className={Styles.orderDetailLabel}>Discount: </p><span className={Styles.orderDetailValue} >{orderValues.discountAmount}</span></div>
                        :
                        null
                    }
                    <div className={Styles.orderDetail}><p className={Styles.orderDetailLabel}>Tax: </p><span className={Styles.orderDetailValue} >₹ {numberWithCommas(orderValues.tax)}</span></div>
                    <div className={Styles.orderDetail}><p className={Styles.orderDetailLabel}>Bill Amount: </p><span className={Styles.orderDetailValue} >₹ {numberWithCommas(orderValues.billAmount)}</span></div>
                    
                    <CouponInput  coupon={coupon} setCoupon={setCoupon} />

                    <Button disabled={processingOrder || loading} onClick={handleCheckout} className={Styles.checkOutButton} variant="primary" id="button-addon2">
                        {
                            processingOrder || loading
                            ?
                            <Spinner size='sm' animation="grow" />
                            :
                            "Checkout"
                        }
                        
                    </Button>
                </div>
            </div>
            <FloatingAlert alertMessage={alertText} setAlertText={setAlertText} />
            <OrderPlaced visibility={orderPlaced} setVisibility={setOrderPlaced} />
        </div>
    )
}

export default CartPage;