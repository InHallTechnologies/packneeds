import React, { useState, useEffect, useRef } from 'react';
import { firebaseAuth } from '../../Backend/firebaseHandler';
import { RecaptchaVerifier, signInWithPhoneNumber } from 'firebase/auth';
import Navigation from '../../Components/Navigation.component';
import Styles from '../../styles/Auth.module.scss';
import { useRouter } from 'next/router';
import FloatingAlert from '../../Components/Alert.component';

const AuthPage = () => {
    const [credentials, setCredentials] = useState({phoneNumber:'', otp:''}); 
    const [otpSent, setOtpSent] = useState(false);
    const [loading, setLoading] = useState(true);
    const signInButton = useRef();
    const [alertText, setAlertText] = useState({message:'', type:""});
    const [confirmationResult, setConfirmationResult] = useState();
    const router = useRouter();
   
    
    useEffect(() => {
        
        window.recaptchaVerifier = new RecaptchaVerifier(signInButton.current, {
            'size': 'invisible',
            'callback': (response) => {
            }
          }, firebaseAuth);
        window.recaptchaVerifier.render();
        
        if (firebaseAuth.currentUser){
            router.push('/')
        }else {
            setLoading(false);
        }
       
    }, [])

    const handleSignUp = () => {
        router.push('signup')
    }


    
    const handleSubmit = () => {
        setLoading(true);
        if (otpSent){
            handleSignIn();
        }else {
            handleSendOtp();
        }
        
        
    }

    const handleSendOtp = () => {
        const appVerifier = window.recaptchaVerifier;
        if (credentials.phoneNumber.length === 10){
            signInWithPhoneNumber(firebaseAuth, "+91"+credentials.phoneNumber, appVerifier).then( result => {
                setOtpSent(true);
                setAlertText({message:`OTP sent to +91${credentials.phoneNumber}`, type:'success'})
                setLoading(false);
                setConfirmationResult(result);
            }).catch(err =>{
                setAlertText({message:"Something went wrong", type:'danger'});
                setLoading(false);
            })
        }else {
            setAlertText({message:"Please enter a valid 10 digit phone number", type:'danger'});
            setLoading(false);
        }
        
    }

    const handleSignIn = () => {
        confirmationResult.confirm(credentials.otp).then(result => {
            setAlertText({message:"Login Successful", type:'success'})
           
            router.push('/');
        }).catch(err => {
            setAlertText({message:"Invalid OTP", type:'danger'})
            setLoading(false);
         
        })
    }
    
    return(
        <div className={Styles.container}>
            <Navigation />
            <div className={Styles.content}>
                <h2 className={Styles.headline}>Login to continue</h2>
                <span className={Styles.subTitle}>Enter your 10 digit phone number to continue</span>
                <input disabled={otpSent?true:false} className={Styles.phoneNumberInput} type='tel' value={credentials.phoneNumber} onChange={event => setCredentials({...credentials, phoneNumber: event.target.value})} placeholder="+91 XXXXXXXXXX" />
                {
                    otpSent
                    ?
                    <input className={Styles.phoneNumberInput}  value={credentials.otp} onChange={event => setCredentials({...credentials, otp: event.target.value})} placeholder="Enter OTP here" />
                    :
                    null
                }

                <button ref={signInButton} onClick={handleSubmit} className={Styles.sendOtpButton}>
                    {
                        !loading
                        ?
                        <span>{otpSent?"Verify OTP":"Send OTP"}</span>
                        :
                        <img src='/loader.gif' className={Styles.loadingImage} />
                    }
                    
                </button>
                
                <span className={Styles.signUpText}>Dont have an account? <u className={Styles.signUpButton} onClick={handleSignUp} ><strong>SignUp</strong></u></span>
            </div>

            {
                alertText.message
                ?
                <FloatingAlert alertMessage={alertText} setAlertText={setAlertText} />
                :
                null
            }
            
        </div>
    )
}

export default AuthPage