import React, { useState, useEffect } from "react";
import Navigation from "../../Components/Navigation.component";
import Styles from "../../styles/MyAccount.module.scss";
import { Form, Col, Row, Button, Spinner } from "react-bootstrap";
import Footer from "../../Components/Footer.component";
import FloatingAlert from "../../Components/Alert.component";
import { ref, set } from 'firebase/database';
import { firebaseDatabase } from "../../Backend/firebaseHandler";
import Head from 'next/head';

const MyAccount = ({ userData, setUserData }) => {
    const [newUserdata, setNewUserData] = useState({ ...userData });
    const [loading, setLoading] = useState(false);
    const [alertText, setAlertText] = useState({message:'', type:''})

    useEffect(() => {
        if (userData) {
            setNewUserData(userData);
        }
    }, [userData]);

    const handleChange = (event) => {
        const { name, value } = event.target;
        setNewUserData({...newUserdata, [name]: value})
    }

    const handleSubmit = async () => {
        if (!newUserdata.name){
            setAlertText({message:'Please enter your name',type:'danger'});
            return;
        }

        if (!newUserdata.address){
            setAlertText({message:'Please enter your address',type:'danger'});
            return;
        }

        if (!newUserdata.city){
            setAlertText({message:'Please enter your city',type:'danger'});
            return;
        }

        if (!newUserdata.state){
            setAlertText({message:'Please enter your state',type:'danger'});
            return;
        }

        if (!newUserdata.pincode){
            setAlertText({message:'Please enter your 6 digit pincode',type:'danger'});
            return;
        }

        setLoading(true);
        const userRef = ref(firebaseDatabase, `USER_ARCHIVE/${newUserdata.phoneNumber}/`);
        await set(userRef, newUserdata);
        setUserData(newUserdata);
        setLoading(false);
        setAlertText({message:"Changes saved successfully", type:'success'})
    }

    if (userData) {
        return (
            <div className={Styles.container}>
                <Head>
                    <title>My Account | Pack Needs</title>
                </Head>
                <Navigation />
                <div className={Styles.nameContainer}>
                    <span>Welcome</span>
                    <h1>{newUserdata.name}</h1>
                </div>

                <div className={Styles.formContent}>
                    <Form>
                        <h2 className={Styles.sectionTitles}>Personal Details</h2>

                        <Form.Group
                            as={Row}
                            className="mb-3"
                            controlId="formPlaintextEmail"
                        >
                            <Form.Label column sm="2">
                                Name
                            </Form.Label>
                            <Col sm="10">
                                <Form.Control
                                    type="text"
                                    placeholder="name"
                                    value={newUserdata.name}
                                    name="name"
                                    onChange={handleChange}
                                />
                            </Col>
                        </Form.Group>

                        <Form.Group
                            as={Row}
                            className="mb-3"
                            controlId="formPlaintextEmail"
                        >
                            <Form.Label column sm="2">
                                Phone Number
                            </Form.Label>
                            <Col sm="10">
                                <Form.Control
                                    type="tel"
                                    placeholder="+91 xxxxxxxxxx"
                                    value={newUserdata.phoneNumber}
                                    name="phoneNumber"
                                    disabled
                                />
                            </Col>
                        </Form.Group>

                        <Form.Group
                            as={Row}
                            className="mb-3"
                            controlId="formPlaintextEmail"
                        >
                            <Form.Label column sm="2">
                                Alternate Phone Number
                            </Form.Label>
                            <Col sm="10">
                                <Form.Control
                                    type="tel"
                                    placeholder="+91 xxxxxxxxxx"
                                    value={newUserdata.alternatePhoneNumber}
                                    name="alternatePhoneNumber"
                                    onChange={handleChange}
                                />
                            </Col>
                        </Form.Group>

                        <Form.Group
                            as={Row}
                            className="mb-3"
                            controlId="formPlaintextEmail"
                        >
                            <Form.Label column sm="2">
                                Email Id
                            </Form.Label>
                            <Col sm="10">
                                <Form.Control
                                    type="email"
                                    placeholder="someone@example.com"
                                    value={newUserdata.emailId}
                                    name="emailId"
                                    onChange={handleChange}
                                />
                            </Col>
                        </Form.Group>
                    </Form>
                </div>


                <div className={Styles.formContent}>
                    <Form>
                        <h2 className={Styles.sectionTitles}>Company Details</h2>

                        <Form.Group
                            as={Row}
                            className="mb-3"
                            controlId="formPlaintextEmail"
                        >
                            <Form.Label column sm="2">
                                Company Name
                            </Form.Label>
                            <Col sm="10">
                                <Form.Control
                                    type="text"
                                    placeholder=""
                                    value={newUserdata.companyName}
                                    name="companyName"
                                    onChange={handleChange}
                                />
                            </Col>
                        </Form.Group>

                        <Form.Group
                            as={Row}
                            className="mb-3"
                            controlId="formPlaintextEmail"
                        >
                            <Form.Label column sm="2">
                                GST
                            </Form.Label>
                            <Col sm="10">
                                <Form.Control
                                    type="text"
                                    placeholder=""
                                    value={newUserdata.gst}
                                    name="gst"
                                    onChange={handleChange}
                                />
                            </Col>
                        </Form.Group>
                    </Form>
                </div>


                <div className={Styles.formContent}>
                    <Form>
                        <h2 className={Styles.sectionTitles}>Address Details</h2>

                        <Form.Group
                            as={Row}
                            className="mb-3"
                            controlId="formPlaintextEmail"
                        >
                            <Form.Label column sm="2">
                                Address
                            </Form.Label>
                            <Col sm="10">
                                <Form.Control
                                    type="text"
                                    placeholder=""
                                    value={newUserdata.address}
                                    name="address"
                                    onChange={handleChange}
                                    as="textarea" rows={3}
                                />
                            </Col>
                        </Form.Group>

                        <Form.Group
                            as={Row}
                            className="mb-3"
                            controlId="formPlaintextEmail"
                        >
                            <Form.Label column sm="2">
                                City
                            </Form.Label>
                            <Col sm="10">
                                <Form.Control
                                    type="text"
                                    placeholder=""
                                    value={newUserdata.city}
                                    name="city"
                                    onChange={handleChange}
                                />
                            </Col>
                        </Form.Group>

                        <Form.Group
                            as={Row}
                            className="mb-3"
                            controlId="formPlaintextEmail"
                        >
                            <Form.Label column sm="2">
                                State
                            </Form.Label>
                            <Col sm="10">
                                <Form.Control
                                    type="text"
                                    placeholder=""
                                    value={newUserdata.state}
                                    name="state"
                                    onChange={handleChange}
                                />
                            </Col>
                        </Form.Group>

                        <Form.Group
                            as={Row}
                            className="mb-3"
                            controlId="formPlaintextEmail"
                        >
                            <Form.Label column sm="2">
                                Pin Code
                            </Form.Label>
                            <Col sm="10">
                                <Form.Control
                                    type="number"
                                    placeholder=""
                                    value={newUserdata.pincode}
                                    name="pincode"
                                    onChange={handleChange}
                                />
                            </Col>
                        </Form.Group>
                    </Form>

                </div>
                
                <Button onClick={handleSubmit} disabled={loading} className={Styles.submitButton} variant="primary">
                    {
                        loading
                        ?
                        <Spinner animation="grow" size='sm' />
                        :
                        "Save Changes"
                    }
                    
                </Button>
                <FloatingAlert alertMessage={alertText} setAlertText={setAlertText} />
                <Footer />
            </div>
        );
    } else {
        return (
            <div>
                <Head>
                    <title>My Account | Pack Needs</title>
                </Head>
            </div>
        );
    }
};

export default MyAccount;
