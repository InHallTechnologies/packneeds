import React, { useEffect, useRef, useState } from "react";
import Navigation from "../../Components/Navigation.component";
import { ref, onValue } from "firebase/database";
import { firebaseDatabase } from "../../Backend/firebaseHandler";
import { InputGroup, FormControl } from "react-bootstrap";
import Styles from "../../styles/Orders.module.scss";
import { BsSearch } from "react-icons/bs";
import OrderArch from "../../Components/OrderArch.component";
import Head from 'next/head';
import Footer from "../../Components/Footer.component";

const MyOrder = ({ userData }) => {
    const [orderList, setOrderList] = useState([]);
    const [emptyList, setEmptyList] = useState(false);
    const [loading, setLoading] = useState(true);
    const [searchQuery, setSearchQuery] = useState("");
    const allOrdersList = useRef();

    useEffect(() => {
        
        (async () => {
            if (userData) {
                const orderRef = ref(
                    firebaseDatabase,
                    `ORDER_ARCHIVE/${userData.phoneNumber}`
                );
                onValue(orderRef, (orderSnapshot) => {
                    if (orderSnapshot.exists()) {
                        const data = [];
                        for (const key in orderSnapshot.val()) {
                            const order = orderSnapshot.child(key).val();
                            data.push(order);
                        }
                        data.reverse()
                        setOrderList(data);
                        allOrdersList.current = data;
                    } else {
                        setEmptyList(true);
                    }
                    setLoading(false);
                });
            }
        })();
    }, [userData]);

    useEffect(() => {
        if (allOrdersList.current) {
            const data = [];
            for (const order of allOrdersList.current) {
                if (JSON.stringify(order).toUpperCase().includes(searchQuery.toUpperCase())) {
                    data.push(order);
                }
            }
            setOrderList(data);
        }
    }, [searchQuery]);

    return (
        <div className={Styles.container}>
            <Head>
                <title>My Orders | Pack Needs</title>
            </Head>
            <Navigation currentActive="orders" />

            <div className={Styles.topSection}>
                <h1 className={Styles.sectionTitle}>Order List</h1>
                <div className={Styles.searchContainer}>
                    <InputGroup  className="mb-3">
                        <InputGroup.Text id="basic-addon1">
                            <BsSearch size="15" color="#444" />
                        </InputGroup.Text>
                        <FormControl
                            placeholder="Search in orders"
                            aria-label="search in orders"
                            aria-describedby="basic-addon1"
                            value={searchQuery}
                            onChange={(event) =>
                                setSearchQuery(event.target.value)
                            }
                        />
                    </InputGroup>
                </div>
            </div>

            <div className={Styles.orderList}>
                {orderList.map((item) => (
                    <OrderArch key={item.orderId} order={item} />
                ))}
            </div>
            <Footer />
        </div>
    );
};

export default MyOrder;
