import React, { useEffect, useState } from 'react';
import serverType from '../../Backend/serverType';
import Navigation from '../../Components/Navigation.component';
import ProductImageSelector from '../../Components/ProductImageSelector.component';
import Styles from '../../styles/ProductDetail.module.scss';
import { InputGroup, FormControl, Button, Spinner } from 'react-bootstrap';
import Head from 'next/head';
import { FaShoppingCart } from 'react-icons/fa';
import RecommendedProducts from '../../Components/RecomendedProducts.component';
import Footer from '../../Components/Footer.component';
import FloatingAlert from '../../Components/Alert.component';
import { set, ref } from 'firebase/database';
import { firebaseDatabase } from '../../Backend/firebaseHandler';

const ProductDetail = ({ userData, product  }) => {
    const { productImages, name, adminsPrice, description, category, moq, key } = product;
    const [quantity, setQuantity] = useState(moq);
    const [note, setNote] = useState("");
    const [addingToCard, setAddingToCart] = useState(false);
    const [alertText, setAlertText] = useState({message:"", type:""});

    useEffect(() => {
        
        if (parseFloat(quantity) <= 0){
            setQuantity(moq);
        }
       
    }, [quantity]);

    const handleAddedToCard = async () => {
        if (!userData.phoneNumber){
            setAlertText({message:"Please login first", type:"danger"});
            return;
        }

        if (parseFloat(quantity) <parseFloat(moq)){
            setAlertText({message:`Please enter a quantity greater than or equal to MOQ (${moq})` , type:"danger"});
            return
        }

        setAddingToCart(true);
        const cartRef = ref(firebaseDatabase, `CART_ARCHIVE/${userData.phoneNumber}/${key}`);
        await set(cartRef, { ...product, orderUnits: quantity, sellerInstructions: note });
        setTimeout(() => {
            setAlertText({message:`Added ${quantity} units of "${name}" to cart`, type:'success'})
            setAddingToCart(false);
        }, 1200)
        

    }

    return(
        <div className={Styles.container}>
            <Head>
                <title>{name} | {category} | Pack Needs</title>
            </Head>
            <header>
                <Navigation />
            </header>

            <main>
                <div className={Styles.infoContainer}>
                    <div className={Styles.imageListContainer}>
                        <ProductImageSelector productName={name} imageList={productImages}  />
                    </div>

                    <div className={Styles.productInfoContainer}>
                        <h2 className={Styles.productName}>{name.toUpperCase()}</h2>
                        <span className={Styles.categoryLabel}>{category}</span>

                        <div className={Styles.descriptionContainer}>
                            <label style={{fontSize:"1.5rem"}} ><strong>Unit Price</strong></label>
                            <p style={{fontSize:"1.3rem"}}>₹ {adminsPrice}</p>
                        </div>

                        <div className={Styles.descriptionContainer}>
                            <label><strong>Description</strong></label>
                            <p>{description}</p>
                        </div>

                       

                        <div className={Styles.quantityContainer}>
                            <label><strong>Quantity (MOQ: {moq})</strong></label>
                            <div className={Styles.quantitySelectorContainer}>
                                <InputGroup style={{width:'170px'}} className="mb-3">
                                <FormControl
                                    placeholder="Quantity"
                                    aria-label="Quantity"
                                    aria-describedby="basic-addon1"
                                    value={quantity}
                                    type='number'
                                    onChange={event => setQuantity(event.target.value)}
                                    />

                                </InputGroup>

                                <span className={Styles.price} > ₹ {(parseFloat(adminsPrice) * parseFloat(quantity)).toFixed(2)}</span>
                            </div>
                        </div>


                        <div className={Styles.quantityContainer}>
                            <label><strong>Add Note (Optional)</strong></label>
                            <div className={Styles.quantitySelectorContainer}>
                                <InputGroup  className="mb-3">
                                <FormControl
                                    placeholder="Add note for seller"
                                    aria-label="Add note for seller"
                                    aria-describedby="basic-addon1"
                                    value={note}
                                    as="textarea"
                                    type='text'
                                    onChange={event => setNote(event.target.value)}
                                    style={{resize:'none', minHeight:'120px'}}
                                    />
                                </InputGroup>                            
                            </div>
                        </div>

                        <Button onClick={handleAddedToCard} className={Styles.addToCart} disabled={addingToCard} style={{backgroundColor:'#CF4539', border:'none'}} >
                            {
                                addingToCard
                                ?
                                <Spinner v size='sm' animation="grow" />
                                :
                                <FaShoppingCart size={20} color='white' />
                            }
                            
                            {
                                addingToCard
                                ?
                                null
                                :
                                <span className={Styles.cartLabel}>Add To Cart</span>
                            }
                            
                        </Button>
                    </div>
                </div>

                <RecommendedProducts category={category} />
                <FloatingAlert alertMessage={alertText} setAlertText={setAlertText} />
            </main>
                            
            <Footer />

            
        </div>
    )
}


export async function getServerSideProps(context) {
    const { productId } = context.query
    
    const url = `${serverType}api/getProductDetail?productId=${productId}`
    const response = await fetch(url);
    const { product } = await response.json();
    
    return {
      props: {
          product
      }, // will be passed to the page component as props
    }
}

export default ProductDetail