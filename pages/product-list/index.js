import React from 'react';
import serverType from '../../Backend/serverType';
import MainProduct from '../../Components/MainProduct.component';
import Navigation from '../../Components/Navigation.component';
import Styles from  '../../styles/ProductList.module.scss';
import Head from 'next/head';
import Footer from '../../Components/Footer.component';
import { useRouter } from 'next/router';

const ProductList = ({ userData, productList, type}) => {

    const router = useRouter();

    const visitProduct = (id) => {
        router.push(`product-detail?productId=${id}`)
    }

    return(
        <div className={Styles.container}>
            <Head>
                <title>{type} | Pack Needs</title>
            </Head>
            <header>
                <Navigation />
            </header>

            <main>
                <h1 className={Styles.typeLabel}>{type}</h1>
                <div className={Styles.productList}>
                    
                    {
                        productList.map(item => <MainProduct visitProduct={visitProduct} key={item.key} product={item} />)
                    }
                </div>
            </main>

            <Footer />
        </div>
    )
}


export async function getServerSideProps(context) {
    const { type }  = context.query
    const url = `${serverType}api/getProductList?type=${type}`
    const response = await fetch(url);
    const { productList } = await response.json();
    
    return {
      props: {
          productList,
          type

      }, // will be passed to the page component as props
    }
  }

export default ProductList;