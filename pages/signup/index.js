import React, { useEffect, useRef, useState } from "react";
import userDetails from "../../Backend/userDetails";
import AddressDetails from "../../Components/AddressDetails.component";
import CompanyDetails from "../../Components/CompanyDetails.component";
import Navigation from "../../Components/Navigation.component";
import PersonalDetails from "../../Components/PersonalDetails.component";
import Styles from "../../styles/Signup.module.scss";
import Button from 'react-bootstrap/Button';
import FloatingAlert from "../../Components/Alert.component";
import OtpVerificationModal from "../../Components/OTPVerification.component";
import { signInWithPhoneNumber, RecaptchaVerifier, updateProfile } from 'firebase/auth';
import { set, ref, get } from 'firebase/database';
import { firebaseAuth, firebaseDatabase } from "../../Backend/firebaseHandler";
import validator from "validator";
import AccountExists from "../../Components/AccountExists.component";
import { useRouter } from "next/router";
import Head from 'next/head';

const SignUp = () => {
    const [currentTab, setCurrentTab] = useState("personal");
    const [userData, setUserData] = useState({...userDetails})
    const [alertMessage, setAlertMessage] = useState({message:'', type:''});
    const [loading, setLoading] = useState(false);
    const [otpSent, setOtpSent] = useState(false);
    const [accountExists, setAccountExists] = useState(false);
    const signInButton = useRef();
    const confirmationResultRef = useRef();
    const router = useRouter();

    useEffect(() => {
        window.recaptchaVerifier = new RecaptchaVerifier(signInButton.current, {
            'size': 'invisible',
            'callback': (response) => {
            }
          }, firebaseAuth);
    }, [])

    const handleSubmit = () => {
        if (!userData.name){
            setAlertMessage({message:'Please enter your name in Personal Details', type:'danger'})
            return;
        }

        if (!validator.isMobilePhone(`+91${userData.phoneNumber}`)){
            setAlertMessage({message:'Please enter a valid 10 digit phone number in Personal Details', type:'danger'})
            return;
        }

        if (!userData.address){
            setAlertMessage({message:'Please enter your address in Address Details', type:'danger'})
            return;
        }

        if (!userData.city){
            setAlertMessage({message:'Please enter your city in Address Details', type:'danger'})
            return;
        }

        if (!userData.state){
            setAlertMessage({message:'Please enter your state in Address Details', type:'danger'})
            return;
        }

        if (!userData.pincode){
            setAlertMessage({message:'Please enter your pincode in Address Details', type:'danger'})
            return;
        }
        
        if (!otpSent){
            sendOtp();
        }

    }

    const sendOtp = () => {
        setLoading(true);
        const appVerifier = window.recaptchaVerifier;
        signInWithPhoneNumber(firebaseAuth, `+91${userData.phoneNumber}`, appVerifier)
        .then((confirmationResult) => {
            window.confirmationResult = confirmationResult;
            confirmationResultRef.current = confirmationResult;
            setOtpSent(true);
            setLoading(false);
        }).catch((error) => {
            setLoading(false);
            setAlertMessage({message:"Cannot send otp. Please check your internet connection o try again later", type:'danger'})
        });
    }

    const handleSuccessLogin = async () => {

        const firebaseProfileRef = ref(firebaseDatabase, `USER_ARCHIVE/+91${userData.phoneNumber}`);
        const userProfileStatus = await get(firebaseProfileRef)
        if (!userProfileStatus.exists()){
            await set(firebaseProfileRef, userData);
            await updateProfile(firebaseAuth.currentUser, {displayName: userData.name})
            setAlertMessage({message:"Successfully created account", type:"success"})
            setTimeout(() => {
                router.push('/')
            }, 1000);
        }else {
            setAccountExists(true);
        }
        
    }   
    
    return(
        <div className={Styles.container}>
            <Head>
                <title>Create Account | Pack Needs</title>
            </Head>
            <header>
                <Navigation />
            </header>
            
            <main>
                <div className={Styles.paginationContainer}>
                    <span className={`${Styles.option} ${currentTab==='personal'?Styles.active:null}`} onClick={_ => setCurrentTab("personal")} >Personal Details</span>
                    <span className={`${Styles.option} ${currentTab==='company'?Styles.active:null}`} onClick={_ => setCurrentTab("company")} >Company Details</span>
                    <span className={`${Styles.option} ${currentTab==='address'?Styles.active:null}`} onClick={_ => setCurrentTab("address")} >Address Details</span>
                </div>

                {
                    currentTab === "personal"
                    ?
                    <PersonalDetails userData={userData} setUserData={setUserData} />
                    :
                    null
                }

                {
                    currentTab === 'company'
                    ?
                    <CompanyDetails userData={userData} setUserData={setUserData} />
                    :
                    null
                }

                {
                    currentTab === 'address'
                    ?
                    <AddressDetails userData={userData} setUserData={setUserData} />
                    :
                    null
                }

                <p className={Styles.tnc}>By continuing you agree to our <u className={Styles.tncLabel}><strong>Terms and Condition</strong></u></p>
                <Button ref={signInButton} className={Styles.button} onClick={handleSubmit} >
                    {
                        loading
                        ?
                        <img className={Styles.loader} src='loader.gif' />
                        :
                        <span>Sign Up</span>
                    }
                    
                </Button>

                
            </main>

            {
                alertMessage.message 
                ?
                <FloatingAlert alertMessage={alertMessage} setAlertText={setAlertMessage} />
                :
                null
            }
            
            <OtpVerificationModal visibility={otpSent} setVisibility={setOtpSent} phoneNumber={userData.phoneNumber} name={userData.name} handleSuccessLogin={handleSuccessLogin} confirmation={confirmationResultRef.current}  />
            <AccountExists visibility={accountExists} setVisibility={setAccountExists} phoneNumber={userData.phoneNumber} />
        </div>
    )
}

export default SignUp;